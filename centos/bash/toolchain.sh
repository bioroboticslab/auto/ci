# GCC 10
source scl_source enable devtoolset-10

# Git 2.18
source scl_source enable rh-git218

export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:/usr/lib/pkgconfig:/usr/local/lib/pkgconfig"
