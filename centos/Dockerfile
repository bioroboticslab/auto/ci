# SPDX-License-Identifier: GPL-3.0-or-later
FROM centos:7

LABEL maintainer="Moritz Maxeiner <moritz.maxeiner@fu-berlin.de>"
LABEL authors="Moritz Maxeiner <moritz.maxeiner@fu-berlin.de>"

ENV BASH_ENV="/etc/profile"
RUN yum install -y \
        centos-release-scl \
        epel-release && \
    yum update -y && \
    yum clean all

# CUDA
RUN yum install -y dkms && \
    rpm -ivh https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-repo-rhel7-10.1.243-1.x86_64.rpm && \
    yum install -y cuda-10-1 && \
    rpm -ivh http://developer.download.nvidia.com/compute/machine-learning/repos/rhel7/x86_64/nvidia-machine-learning-repo-rhel7-1.0.0-1.x86_64.rpm && \
    yum install -y libcudnn7-devel-7.6.5.32-1.cuda10.1 && \
    yum clean all && \
    tar -C /usr/local/cuda-10.2 -ac . | tar -C /usr/local/cuda-10.1/ -x && \
    rm -rf /usr/local/cuda-10.2

# GCC
RUN yum install -y \
        kernel && \
    yum install -y \
        devtoolset-8 \
        devtoolset-10 && \
    yum install -y \
        rh-git218-git \
        patch && \
    yum clean all
ADD bash/toolchain.sh /etc/profile.d/toolchain.sh
ADD bash/ld-library-path.sh /etc/profile.d/ld-library-path.sh

# CMake
ARG CMake_VERSION=3.21.2
RUN yum install -y \
        curl-devel \
        expat-devel \
        zlib-devel \
        bzip2-devel \
        xz-devel \
        libzstd-devel \
        rhash-devel \
        libuv-devel \
        && \
    yum clean all && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://cmake.org/files/v${CMake_VERSION%.*}/cmake-${CMake_VERSION}.tar.gz && \
    tar -xf cmake-${CMake_VERSION}.tar.gz && \
    cd cmake-${CMake_VERSION} && \
    ./bootstrap --system-libs --no-system-libarchive --no-system-jsoncpp  --prefix=/usr/local && \
    make install -j$(nproc --all) && \
    cd .. && \
    rm -rf cmake-${CMake_VERSION}*

# Ninja
RUN yum install -y ninja-build && \
    ln -s /usr/bin/ninja-build /usr/local/bin/ninja && \
    yum clean all

# Python 3.6
RUN yum install -y \
        python36-devel \
        python36-numpy \
        && \
    yum clean all

# FFmpeg
ARG FFmpeg_VERSION=4.2.4
RUN yum install -y \
        nasm \
        libass-devel \
        libtheora-devel \
        libvorbis-devel \
        freetype-devel  \
        && \
    yum clean all && \
    source /etc/profile && \
    source scl_source enable devtoolset-8 && \
    cd /tmp && \
    git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git && \
    cd nv-codec-headers && \
    make install && \
    cd .. && \
    rm -rf nv-codec-headers && \
    curl -sSLO http://ffmpeg.org/releases/ffmpeg-${FFmpeg_VERSION}.tar.gz && \
    tar -xf ffmpeg-${FFmpeg_VERSION}.tar.gz && \
    cd ffmpeg-${FFmpeg_VERSION} && \
    ./configure --prefix=/usr/local --enable-gpl --enable-nvenc --enable-libfreetype --enable-avresample --enable-shared --disable-static --enable-pic && \
    make install && \
    cd .. && \
    rm -rf ffmpeg-${FFmpeg_VERSION}*

# HDF
ARG HDF_VERSION=1.12.0
RUN yum install -y \
        bzip2 \
        && \
    yum clean all && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-${HDF_VERSION%.*}/hdf5-${HDF_VERSION}/src/hdf5-${HDF_VERSION}.tar.bz2 && \
    tar -xf hdf5-${HDF_VERSION}.tar.bz2 && \
    cd hdf5-${HDF_VERSION} && \
    cmake -S. -B build  -G Ninja \
        -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DBUILD_SHARED_LIBS=OFF \
        && \
    ninja -C build install && \
    cd .. && \
    rm -rf hdf5-${HDF_VERSION}*

# Qt
ARG Qt5_VERSION=5.15.2
RUN yum install -y \
        alsa-lib-devel \
        at-spi2-atk-devel \
        bison \
        bluez-libs-devel \
        clang-devel \
        cups-devel \
        dbus-devel \
        double-conversion-devel \
        egl-wayland-devel \
        flex \
        flite-devel \
        fontconfig-devel \
        freeglut-devel \
        freetype-devel \
        gperf \
        gstreamer-plugins-base-devel \
        gstreamer1-devel \
        gstreamer1-plugins-base-devel \
        gtk3-devel \
        harfbuzz-devel \
        jasper-devel \
        libcap-devel \
        libdc1394-devel \
        libicu-devel \
        libinput-devel \
        libjpeg-turbo-devel \
        libmng-devel \
        libpng-devel \
        libticonv-devel \
        libtiff-devel \
        libv4l-devel \
        libwebp-devel \
        libxcb \
        libxcb-devel \
        libXcomposite-devel \
        libXcursor-devel \
        libxkbcommon-devel \
        libxkbcommon-x11-devel \
        libXrandr-devel \
        libxslt-devel \
        libXtst-devel \
        libzstd-devel \
        llvm-devel \
        mesa-libEGL-devel \
        mesa-libgbm-devel \
        mesa-libGL-devel \
        nspr-devel \
        nss-devel \
        openssl11-devel \
        pciutils-devel \
        pcre2-devel \
        postgresql-devel \
        pulseaudio-libs-devel \
        pulseaudio-libs-glib2 \
        ruby \
        speech-dispatcher-devel \
        tesseract-devel \
        tslib-devel \
        vulkan-devel \
        wayland-devel \
        xcb-util \
        xcb-util-devel \
        xcb-util-image-devel \
        xcb-util-keysyms-devel \
        xcb-util-renderutil-devel \
        xcb-util-wm-devel \
        && \
    yum clean all && \
    source /etc/profile && \
    source scl_source enable devtoolset-8 && \
    cd /opt && \
    curl -sSLO https://download.qt.io/official_releases/qt/${Qt5_VERSION%.*}/${Qt5_VERSION}/single/qt-everywhere-src-${Qt5_VERSION}.tar.xz && \
    tar -xf qt-everywhere-src-${Qt5_VERSION}.tar.xz && \
    cd qt-everywhere-src-${Qt5_VERSION} && \
    mkdir build && \
    cd build && \
    ../configure -prefix "/opt/qt5/${Qt5_VERSION}" -opensource -confirm-license -no-feature-qdoc -skip qtwebengine -xcb -xcb-xlib -xcb-native-painting -openssl OPENSSL_INCDIR=/usr/include/openssl11 OPENSSL_LIBDIR=/usr/lib64/openssl11 && \
    make -j $(nproc) && \
    make install && \
    cd ../.. && \
    rm -rf qt-everywhere-src-${Qt5_VERSION}* && \
    echo "/opt/qt5/${Qt5_VERSION}/lib" > /etc/ld.so.conf.d/qt5.conf
ENV Qt5_DIR=/opt/qt5/${Qt5_VERSION}/lib/cmake/Qt5

# linuxdeployqt
RUN version=6 && \
    yum install -y \
        desktop-file-utils \
        && \
    cd /opt && \
    curl -sSLO https://github.com/probonopd/linuxdeployqt/releases/download/${version}/linuxdeployqt-${version}-x86_64.AppImage && \
    chmod +x linuxdeployqt-${version}-x86_64.AppImage && \
    ./linuxdeployqt-${version}-x86_64.AppImage --appimage-extract && \
    rm -f linuxdeployqt-${version}-x86_64.AppImage && \
    mv squashfs-root linuxdeployqt-${version} && \
    ln -s /opt/linuxdeployqt-${version}/AppRun /usr/local/bin/linuxdeployqt

# Boost
ARG Boost_VERSION=1.73.0
RUN cd /tmp && \
    curl -sSLO https://boostorg.jfrog.io/artifactory/main/release/${Boost_VERSION}/source/boost_${Boost_VERSION//./_}.tar.gz && \
    tar -xf boost_${Boost_VERSION//./_}.tar.gz && \
    cd boost_${Boost_VERSION//./_} && \
    ./bootstrap.sh \
        --prefix=/opt/boost && \
    ./b2 install \
        --build-dir=build \
        cxxflags=-fPIC \
        --without-python && \
    cd .. && \
    rm -rf boost_* && \
    echo "export BOOST_ROOT=/opt/boost" > /etc/profile.d/boost.sh && \
    echo -e "/opt/boost/lib" > /etc/ld.so.conf.d/boost.conf && \
    ldconfig

# fmt
ARG fmt_VERSION=7.1.0
RUN cd /tmp && \
    curl -sSLO https://github.com/fmtlib/fmt/releases/download/${fmt_VERSION}/fmt-${fmt_VERSION}.zip && \
    unzip fmt-${fmt_VERSION}.zip && \
    cd fmt-${fmt_VERSION} && \
    cmake -S. -B build -G Ninja \
        -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local \
        && \
    ninja -C build install && \
    cd .. && \
    rm -rf fmt-${fmt_VERSION}*

# OpenCV
ARG OpenCV_VERSION=4.5.0
RUN yum install -y \
        libpng-devel \
        jasper-devel \
        libwebp-devel \
        libjpeg-turbo-devel \
        freeglut-devel \
        mesa-libGL-devel \
        libtiff-devel \
        libdc1394-devel \
        eigen3-devel \
        libv4l-devel \
        openblas-devel \
        lapack-devel \
        gstreamer-plugins-base-devel \
        tesseract-devel \
        harfbuzz-devel \
        && \
    yum clean all && \
    source /etc/profile && \
    source scl_source enable devtoolset-8 && \
    cd /tmp && \
    curl -sSL -o opencv-${OpenCV_VERSION}.zip https://github.com/opencv/opencv/archive/${OpenCV_VERSION}.zip && \
    unzip opencv-${OpenCV_VERSION}.zip && \
    curl -sSL -o opencv_contrib-${OpenCV_VERSION}.zip https://github.com/opencv/opencv_contrib/archive/${OpenCV_VERSION}.zip && \
    unzip opencv_contrib-${OpenCV_VERSION}.zip && \
    cd opencv-${OpenCV_VERSION} && \
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig && \
    cmake -S. -B build -G Ninja \
        -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local \
        -D PYTHON_DEFAULT_EXECUTABLE=/usr/bin/python3 \
        -D OPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib-${OpenCV_VERSION}/modules \
        -D ENABLE_CXX11=ON \
        -D BUILD_EXAMPLES=OFF \
        -D BUILD_PACKAGE=OFF \
        -D BUILD_TESTS=OFF \
        -D BUILD_PERF_TESTS=OFF \
        -D BUILD_DOCS=OFF \
        -D BUILD_opencv_apps= \
        -D BUILD_WITH_DEBUG_INFO=OFF \
        -D ENABLE_PROFILING=OFF \
        -D ENABLE_COVERAGE=OFF \
        -D OPENCV_ENABLE_NONFREE=OFF \
        -D WITH_OPENMP=ON \
        -D WITH_OPENCL=ON \
        -D WITH_IPP=OFF \
        -D WITH_CUDA=ON \
        -D WITH_CUBLAS=ON \
        -D WITH_CUDNN=OFF \
        -D CUDA_ARCH_BIN="61" \
        -D CUDA_ARCH_PTX="30 52 75" \
        -D BUILD_opencv_cudacodec=OFF \
        -D WITH_EIGEN=ON \
        -D WITH_FFMPEG=ON \
        -D WITH_JASPER=ON -D BUILD_JASPER=OFF \
        -D WITH_JPEG=ON -D BUILD_JPEG=OFF \
        -D WITH_LAPACK=ON \
        -D WITH_MATLAB=OFF \
        -D WITH_OPENCLAMDBLAS=OFF \
        -D WITH_OPENEXR=OFF -D BUILD_OPENEXR=OFF \
        -D WITH_OPENGL=ON \
        -D WITH_PNG=ON -D BUILD_PNG=OFF \
        -D WITH_PROTOBUF=ON -D BUILD_PROTOBUF=ON \
        -D WITH_QT=ON \
        -D WITH_TBB=OFF \
        -D WITH_TIFF=ON -D BUILD_TIFF=OFF \
        -D WITH_WEBP=ON -D BUILD_WEBP=OFF \
        -D WITH_ZLIB=ON -D BUILD_ZLIB=OFF \
        -D WITH_V4L=ON \
        -D BUILD_LIST=alphamat,aruco,bgsegm,bioinspired,calib3d,ccalib,core,cudaarithm,cudabgsegm,cudafeatures2d,cudafilters,cudaimgproc,cudalegacy,cudaobjdetect,cudaoptflow,cudastereo,cudawarping,cudev,cvv,datasets,dnn,dnn_objdetect,dnn_superres,dpm,face,features2d,flann,freetype,fuzzy,gapi,hfs,highgui,img_hash,imgcodecs,imgproc,intensity_transform,line_descriptor,mcc,ml,objdetect,optflow,phase_unwrapping,photo,plot,python3,quality,rapid,reg,rgbd,saliency,shape,stereo,stitching,structured_light,superres,surface_matching,text,tracking,video,videoio,videostab,xfeatures2d,ximgproc,xobjdetect,xphoto \
        && \
    ninja -C build install && \
    cd .. && \
    rm -rf opencv-${OpenCV_VERSION}* opencv_contrib-${OpenCV_VERSION}*

# MXNet
ENV MXNet_DIR=/opt/mxnet-1.6.0
ADD patches/mxnet-1.6.0-manual-cuda-arch-flags.patch /opt/mxnet-1.6.0-manual-cuda-arch-flags.patch
RUN version=1.6.0 && \
    source /etc/profile && \
    source scl_source enable devtoolset-8 && \
    cd /opt && \
    curl -sSLO https://github.com/apache/incubator-mxnet/releases/download/${version}/apache-mxnet-src-${version}-incubating.tar.gz && \
    tar -xf apache-mxnet-src-${version}-incubating.tar.gz && \
    rm -f apache-mxnet-src-${version}-incubating.tar.gz && \
    mv apache-mxnet-src-${version}-incubating mxnet-${version} && \
    cd mxnet-${version} && \
    patch -p1 -i /opt/mxnet-1.6.0-manual-cuda-arch-flags.patch && \
    cmake  -S. -B build -G Ninja \
        -D CMAKE_CXX_STANDARD=14 -D USE_CXX14_IF_AVAILABLE=ON \
        -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local \
        -D CMAKE_CUDA_COMPILER=/usr/local/cuda-10.1/bin/nvcc \
        -D USE_CUDA=ON -D USE_CUDNN=ON -D USE_NCCL=OFF \
        -D CUDA_ARCH_FLAGS="-gencode;arch=compute_61,code=sm_61;-gencode;arch=compute_30,code=compute_30;-gencode;arch=compute_52,code=compute_52;-gencode;arch=compute_75,code=compute_75" \
        -D USE_OPENCV=ON -D USE_OPENMP=ON \
        -D USE_LAPACK=ON -D USE_MKL_IF_AVAILABLE=OFF \
        -D USE_DIST_KVSTORE=OFF \
        -D USE_CPP_PACKAGE=ON \
        -D USE_SIGNAL_HANDLER=ON \
        -D ENABLE_CUDA_RTC=ON \
        -D BUILD_CPP_EXAMPLES=OFF \
        -D USE_GPERFTOOLS=OFF \
        -D USE_JEMALLOC=OFF \
        && \
    rm /opt/mxnet-1.6.0-manual-cuda-arch-flags.patch && \
    ninja -C build

# pylon (Basler cameras)
ARG pylon_VERSION=5.2.0.13457
RUN cd /tmp && \
    curl -sSLO https://www2.baslerweb.com/media/downloads/software/pylon_software/pylon-${pylon_VERSION}-x86_64.tar.gz && \
    tar -xf pylon-${pylon_VERSION}-x86_64.tar.gz && \
    cd pylon-${pylon_VERSION}-x86_64 && \
    tar -xf pylonSDK-${pylon_VERSION}-x86_64.tar.gz && \
    mv pylon5 /opt/pylon5 && \
    cd .. && \
    rm -rf pylon-${pylon_VERSION}*
ADD bash/pylon.sh /etc/profile.d/pylon.sh

# Update yum repos to vault.
RUN rm /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-SCLo-scl.repo /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo
ADD patches/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo
RUN yum makecache

# CGAL
ARG CGAL_VERSION=5.2
RUN yum install -y \
        gmp-devel \
        mpfr-devel \
        && \
    yum clean all && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://github.com/CGAL/cgal/releases/download/v${CGAL_VERSION}/CGAL-${CGAL_VERSION}.tar.xz && \
    tar -xf CGAL-${CGAL_VERSION}.tar.xz && \
    cd CGAL-${CGAL_VERSION} && \
    cmake -S. -B build -G Ninja \
        -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DCGAL_INSTALL_LIB_DIR=lib -DCGAL_INSTALL_CMAKE_DIR="lib/cmake/CGAL" \
        -DWITH_CGAL_Qt5=ON \
        && \
    ninja -C build install && \
    cd .. && \
    rm -rf CGAL-${CGAL_VERSION}*

# glm
ARG glm_VERSION=0.9.9.8
RUN cd /tmp && \
    curl -sSLO https://github.com/g-truc/glm/releases/download/${glm_VERSION}/glm-${glm_VERSION}.zip && \
    unzip glm-${glm_VERSION}.zip && \
    mv glm /opt/glm && \
    rm -rf glm* && \
    echo "export glm_DIR=/opt/glm/cmake/glm" > /etc/profile.d/glm.sh

RUN python3.6 -m pip --no-cache-dir install -U pip && \
    python3.6 -m pip --no-cache-dir install 'cryptography<3.4' &&  \
    python3.6 -m pip --no-cache-dir install \
        wheel \
        twine

# Allow finding libraries installed to /usr/local
RUN echo -e "/usr/local/lib\n/usr/local/lib64" > /etc/ld.so.conf.d/local.conf && \
    ldconfig

# Python build dependencies
RUN yum install -y \
        openssl11-devel \
        libffi-devel \
        readline-devel \
        sqlite-devel \
        libarchive-devel \
        && \
    yum clean all

# Python 3.7
RUN version=3.7.12 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    ./configure \
        --prefix=/usr \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        --with-system-expat \
        && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}* && \
    ldconfig

# Python 3.8
RUN version=3.8.19 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    ./configure \
        --prefix=/usr \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        --with-system-expat \
        && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}* && \
    ldconfig

# pybind11
ARG pybind11_VERSION=2.13.5
RUN source /etc/profile && \
    cd /tmp && \
    curl -sSL -o pybind11-${pybind11_VERSION}.tar.gz https://github.com/pybind/pybind11/archive/v${pybind11_VERSION}.tar.gz && \
    tar -xf pybind11-${pybind11_VERSION}.tar.gz && \
    cd pybind11-${pybind11_VERSION} && \
    cmake -S. -B build -G Ninja \
        -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DPYBIND11_TEST=off \
        && \
    ninja -C build install && \
    cd .. && \
    rm -rf pybind11-${pybind11_VERSION}*

# Python 3.9
RUN version=3.9.19 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    ./configure \
        --prefix=/usr \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        --with-system-expat \
        && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}* && \
    ldconfig

# Python 3.10
RUN version=3.10.14 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    ./configure \
        --prefix=/usr \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        --with-system-expat \
        && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}* && \
    ldconfig

# Python 3.12
RUN version=3.12.5 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    ./configure \
        --prefix=/usr \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        --with-system-expat \
        && \
    make -j$(nproc) && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}* && \
    ldconfig

# Python 3.7 packages
RUN python3.7 -m pip --no-cache-dir install -U pip && \
    python3.7 -m pip --no-cache-dir install 'cryptography<3.4' && \
    python3.7 -m pip --no-cache-dir install \
        wheel \
        twine \
        pytest \
        pytest-cov \
        pytest-testmon \
        pre-commit \
        testbook \
        jupyter \
        pandas \
        h5py \
        deprecation

# Python 3.8 packages
RUN python3.8 -m pip --no-cache-dir install -U pip && \
    python3.8 -m pip --no-cache-dir install 'cryptography<3.4' && \
    python3.8 -m pip --no-cache-dir install \
        wheel \
        twine \
        pytest \
        pytest-cov \
        pytest-testmon \
        pre-commit \
        testbook \
        jupyter \
        pandas \
        h5py \
        deprecation

# Python 3.9 packages
RUN python3.9 -m pip --no-cache-dir install -U pip && \
    python3.9 -m pip --no-cache-dir install 'cryptography<3.4' && \
    python3.9 -m pip --no-cache-dir install \
        wheel \
        twine \
        pytest \
        pytest-cov \
        pytest-testmon \
        pre-commit \
        testbook \
        jupyter \
        pandas \
        h5py \
        deprecation

# Python 3.10 packages
RUN python3.10 -m pip --no-cache-dir install -U pip && \
    python3.10 -m pip --no-cache-dir install 'cryptography<3.4' && \
    python3.10 -m pip --no-cache-dir install \
        wheel \
        twine \
        pytest \
        pytest-cov \
        pytest-testmon \
        pre-commit \
        testbook \
        jupyter \
        pandas \
        h5py \
        deprecation

# Python 3.12 packages
RUN python3.12 -m pip --no-cache-dir install -U pip && \
    python3.12 -m pip --no-cache-dir install 'cryptography<3.4' && \
    python3.12 -m pip --no-cache-dir install \
        wheel \
        twine \
        pytest \
        pytest-cov \
        pytest-testmon \
        pre-commit \
        testbook \
        jupyter \
        pandas \
        h5py \
        deprecation

# Additional development dependencies
RUN yum install -y \
        patchelf \
        libepoxy-devel \
        json-devel \
        && \
    yum clean all

# Install portable python dependencies
ADD python/make-python-portable.py /usr/local/bin/make-python-portable.py

# Portable Python 3.8
RUN version=3.8.19 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    CFLAGS="-fPIC" LDFLAGS="-fPIC" ./configure \
        --prefix=/opt/python/3.8 \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}*
RUN source /etc/profile && \
    make-python-portable.py /opt/python/3.8/bin/python3.8
RUN tar -C /opt/python/3.8/ --numeric-owner --xattrs --xattrs-include='*' -cf /opt/python/python-3.8.tar .

# Portable Python 3.10
RUN version=3.10.14 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    CFLAGS="-fPIC" LDFLAGS="-fPIC" ./configure \
        --prefix=/opt/python/3.10 \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}*
RUN source /etc/profile && \
    make-python-portable.py /opt/python/3.10/bin/python3.10
RUN tar -C /opt/python/3.10/ --numeric-owner --xattrs --xattrs-include='*' -cf /opt/python/python-3.10.tar .

# Portable Python 3.12
RUN version=3.12.5 && \
    source /etc/profile && \
    cd /tmp && \
    curl -sSLO https://www.python.org/ftp/python/${version}/Python-${version}.tgz && \
    tar -xf Python-${version}.tgz && \
    cd Python-${version} && \
    sed -i 's/PKG_CONFIG openssl /PKG_CONFIG openssl11 /g' configure && \
    CFLAGS="-fPIC" LDFLAGS="-fPIC" ./configure \
        --prefix=/opt/python/3.12 \
        --enable-optimizations \
        --enable-shared \
        --with-ensurepip=install \
        && \
    make -j$(nproc) && \
    make -j$(nproc) altinstall && \
    cd .. && \
    rm -rf Python-${version}*
RUN source /etc/profile && \
    make-python-portable.py /opt/python/3.12/bin/python3.12
RUN tar -C /opt/python/3.12/ --numeric-owner --xattrs --xattrs-include='*' -cf /opt/python/python-3.12.tar .
