# SPDX-License-Identifier: GPL-3.0-or-later
FROM git.imp.fu-berlin.de:5000/bioroboticslab/auto/ci/windows:latest-base

LABEL maintainer="Moritz Maxeiner <moritz.maxeiner@fu-berlin.de>"
LABEL authors="Moritz Maxeiner <moritz.maxeiner@fu-berlin.de>"

RUN [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; \
    (New-Object Net.WebClient).DownloadFile('https://developer.download.nvidia.com/compute/cuda/10.1/Prod/network_installers/cuda_10.1.243_win10_network.exe', 'C:/cuda_10.1.243_win10_network.exe'); \
    (New-Object Net.WebClient).DownloadFile('https://developer.download.nvidia.com/compute/redist/cudnn/v7.6.5/cudnn-10.1-windows10-x64-v7.6.5.32.zip', 'C:/cudnn-10.1-windows10-x64-v7.6.5.32.zip'); \
    Start-Process C:/cuda_10.1.243_win10_network.exe -NoNewWindow -Wait -ArgumentList '-s nvcc_10.1 cublas_10.1 cublas_dev_10.1 cudart_10.1 cufft_10.1 cufft_dev_10.1 curand_10.1 curand_dev_10.1 cusolver_10.1 cusolver_dev_10.1 cusparse_10.1 cusparse_dev_10.1 npp_10.1 npp_dev_10.1 nvrtc_10.1 nvrtc_dev_10.1 nvml_dev_10.1'; \
    Remove-Item -Force C:/cuda_10.1.243_win10_network.exe; \
    7z x C:/cudnn-10.1-windows10-x64-v7.6.5.32.zip; \
    Copy-Item -Force -Recurse "./cuda/*" "$([Environment]::GetEnvironmentVariable('CUDA_PATH', [EnvironmentVariableTarget]::Machine))"; \
    Remove-Item -Force -Recurse "./cuda"; \
    Remove-Item -Force C:/cudnn-10.1-windows10-x64-v7.6.5.32.zip

RUN cd $env:VCPKG_DIR; \
    Must-Run ./vcpkg install \
        "\"ffmpeg[nvcodec,avresample]\"" \
        "\"opencv4[cuda,ffmpeg,opengl,contrib,ipp,dnn]\""; \
    Remove-Item -Force -Recurse ./downloads; \
    Remove-Item -Force -Recurse ./buildtrees; \
    Remove-Item -Force -Recurse ./packages;

COPY opengl32.dll C:/Windows/System32/opengl32.dll
COPY glu32.dll C:/Windows/System32/glu32.dll
COPY ddraw.dll C:/Windows/System32/ddraw.dll
COPY nvcuda.dll C:/Windows/System32/nvcuda.dll
RUN cd $env:VCPKG_DIR; \
    Must-Run ./vcpkg install \
        "\"mxnet[cpp,opencv,cuda,cudnn]\""; \
    Remove-Item -Force -Recurse ./downloads; \
    Remove-Item -Force -Recurse ./buildtrees; \
    Remove-Item -Force -Recurse ./packages;

RUN cd $env:VCPKG_DIR; \
    Must-Run ./vcpkg install \
        boost-program-options \
        boost-property-tree \
        boost-circular-buffer \
        boost-timer \
        boost-multi-array \
        libepoxy \
        glm \
        cgal \
        fmt \
        jsoncpp \
        websocketpp \
        freetype \
        harfbuzz \
        rpclib \
        eigen3 \
        pybind11 \
        ; \
    Remove-Item -Force -Recurse ./downloads; \
    Remove-Item -Force -Recurse ./buildtrees; \
    Remove-Item -Force -Recurse ./packages;

RUN Must-Run choco install -y --allow-multiple-versions python --version 3.9.1; \
    Must-Run choco install -y --allow-multiple-versions python --version 3.8.7; \
    Must-Run choco install -y --allow-multiple-versions python --version 3.7.9; \
    $env:PATH = "\"$([Environment]::GetEnvironmentVariable('PATH', [EnvironmentVariableTarget]::User));$([Environment]::GetEnvironmentVariable('PATH', [EnvironmentVariableTarget]::Machine))\""; \
    Must-Run /Python39/python -m pip --no-cache-dir install --no-warn-script-location -U pip; \
    Must-Run /Python39/python -m pip --no-cache-dir install --no-warn-script-location 'cryptography<3.4'; \
    Must-Run /Python39/python -m pip --no-cache-dir install --no-warn-script-location \
        wheel \
        twine \
        pytest \
        pytest-cov \
        testbook \
        jupyter \
        pandas \
        h5py \
        ; \
    Must-Run /Python38/python -m pip --no-cache-dir install --no-warn-script-location -U pip; \
    Must-Run /Python38/python -m pip --no-cache-dir install --no-warn-script-location 'cryptography<3.4'; \
    Must-Run /Python38/python -m pip --no-cache-dir install --no-warn-script-location \
        wheel \
        twine \
        pytest \
        pytest-cov \
        testbook \
        jupyter \
        pandas \
        h5py \
        ; \
    Must-Run /Python37/python -m pip --no-cache-dir install --no-warn-script-location -U pip; \
    Must-Run /Python37/python -m pip --no-cache-dir install --no-warn-script-location 'cryptography<3.4'; \
    Must-Run /Python37/python -m pip --no-cache-dir install --no-warn-script-location \
        wheel \
        twine \
        pytest \
        pytest-cov \
        testbook \
        jupyter \
        pandas \
        h5py
